import Vue from 'vue'
import Router from 'vue-router'
import navbar from '@/components/HelloWorld'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [{
    path: '/',
    name: 'navbar',
    component: navbar,
  }],

})
